'use strict'
var gulp = require('gulp')
var $ = require('gulp-load-plugins')()
var connect = require('gulp-connect')
var browserSync = require('browser-sync').create(); 
var sassPaths = ['assets/sass' ]
var scsslint = require('gulp-scss-lint')

gulp.task('webserver', function() {
    connect.server({
        livereload: true,
        port: 8000,
        host: '0.0.0.0'
    });
});
//Static server 
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        ghostMode: {
            clicks: true,
            forms: true,
            scroll: true
        }
    });
});
gulp.task('html', function() {
    gulp.src('*.html')
        .pipe(connect.reload());
});
gulp.task('js', function() {
    gulp.src('*.js')
        .pipe(connect.reload());
});
gulp.task('sass', function() {
    return gulp.src('assets/sass/*.scss')
        .pipe($.sass({
                includePaths: sassPaths
            })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('assets/css'))
        .pipe(connect.reload());
})

gulp.task('scss-lint', function() {
  return gulp.src('/scss/*.scss')
    .pipe(scsslint());
})

gulp.task('watch', ['sass', 'html', 'js'], function() {
    gulp.watch(['*.html'], ['html']);
    gulp.watch(['includes/**/*.html'], ['html']);
    gulp.watch(['assets/sass/**/*.scss'], ['sass','scss-lint']);
    gulp.watch(['assets/js/**/*.js'], ['js']);
});
gulp.task('default', ['sass', 'webserver', 'watch']);
